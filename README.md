# pyrex

`pyrex` is a way to work on things in Jupyter notebooks using both Python and R, all inside a reproducible docker container. Provides different docker containers with jupyter and some common installs for Python and R.

- Available on [devperiscopic DockerHub](https://hub.docker.com/r/devperiscopic/pyrex)

## Overview
Below is a list of "pyrex containers", which are base docker containers available through DockerHub. They include Python and R, along with different combinations of installed packages for each.

***

# Pyrex Containers

| Container | Usage | Dockerfile | Python | R | Description
| :---: | :--- | :---: | :---: | :---: | :--- |
| [classic](https://bitbucket.org/zachbogart/pyrex/src/main/classic/) | `FROM devperiscopic/pyrex:classic` | [Dockerfile](https://bitbucket.org/zachbogart/pyrex/src/main/classic/Dockerfile) | [Pipfile](https://bitbucket.org/zachbogart/pyrex/src/main/classic/Pipfile) | [R Packages](https://bitbucket.org/zachbogart/pyrex/src/main/classic/install_packages.R) | The basics for Python and R. 
| [prepared](https://bitbucket.org/zachbogart/pyrex/src/main/prepared) | `FROM devperiscopic/pyrex:prepared` | [Dockerfile](https://bitbucket.org/zachbogart/pyrex/src/main/prepared/Dockerfile) | [Pipfile](https://bitbucket.org/zachbogart/pyrex/src/main/prepared/Pipfile) | [R Packages](https://bitbucket.org/zachbogart/pyrex/src/main/prepared/install_packages.R) | Copy of `classic` with selected nbextentions pre-installed. 
| [scribe](https://bitbucket.org/zachbogart/pyrex/src/main/scribe) | `FROM devperiscopic/pyrex:scribe` | [Dockerfile](https://bitbucket.org/zachbogart/pyrex/src/main/scribe/Dockerfile) | [Pipfile](https://bitbucket.org/zachbogart/pyrex/src/main/scribe/Pipfile) | [R Packages](https://bitbucket.org/zachbogart/pyrex/src/main/scribe/install_packages.R) | Copy of `prepared`, includes Python GQL client module (pyjanitor omitted due to conflicts) 

***  

## Installing Packages While Running Jupyter in Docker Container
The pyrex containers may be missing some package you really crave. It's easy to install them while running a jupyter notebook. You can execute these in a cell:

Python: 
```
pip3 install <MODULE_NAME>
```

R:
```
install.packages("<PACKAGE_NAME>")
```

- Note: since installs are inside docker, they will be ephemeral and will have to be reinstalled when you rerun a container.

***

## Why pyrex?

Pyrex:
- "Py" for Python
- "r" for R
- "ex" for...executable? I dunno, I thought it was clever

Made with 💖